#!/usr/bin/env python3


import argparse

from points_io.points_io import point_reader, save_points_as_pdb

parser = argparse.ArgumentParser(description="PDB files rescaling tool.")
parser.add_argument('scale', type=float, help="Scaling factor for PDB file.")
parser.add_argument('pdb_file', help="Input PDB file")
parser.add_argument('out_file', help="Rescaled PDB file")
args = parser.parse_args()

points = point_reader(args.pdb_file)
points = points * args.scale
save_points_as_pdb(points, args.out_file)
