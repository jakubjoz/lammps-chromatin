import re



def main():
    fileName = input('nazwa pliku/file name: ')
    boxsize = input('wielkosc pudelka (double nie int) ')
    #fileName = 'poly.pdb'
    #boxsize = 50.0000
    pdbFile = open(fileName, 'r')
    lmpFile = open(fileName + '.dat', 'w+')
    lmpFile.truncate(0)
    atomCounter: int = 0

    lmpFile.write(
         "atoms\n"
        + "bonds\n"
        + "                                              1 atom types\n"
        + "1 bond types\n\n"
        + "0.0000 " + str(boxsize) + " xlo xhi\n"
        + "0.0000 " + str(boxsize) + " ylo yhi\n"
        + "0.0000 " + str(boxsize) + " zlo zhi\n"
        + "\nMasses\n"
        + "\n1 1\n"
        + "\nAtoms\n\n")

    for line in pdbFile:
        atomPosReg = re.compile('\s{3}(\s*\-{0,1}\d{1,3}\.\d{0,3})(\s*\-{0,1}\d{1,3}\.\d{0,3})(\s*\-{0,1}\d{1,3}\.\d{0,3})')
        atomPoz = re.search(atomPosReg, line)

        if atomPoz is not None:
            atomCounter += 1
            atom = atomPoz.group(1) + " " + atomPoz.group(2) + " " + atomPoz.group(3)
            lmpFile.write(str(atomCounter) + ' 1 1 ' + str(atom) + '\n')
    pdbFile.close()
    lmpFile.write("\nBonds\n")

    for i in range(1, atomCounter):
        lmpFile.write("\n" + str(i) + " 1 " + str(i) + " " + str(i+1))

    lmpFile.seek(0, 0)
    lmpFile.write(
        "\n\n"
        +str(atomCounter) + " atoms\n"
        + str(atomCounter-1) + " bonds\n\n")
    lmpFile.close()


if __name__ == '__main__':
    main()
